from __future__ import unicode_literals
from django.db import models
import re

class UserManager(models.Manager):
    def create_validator(self, reqPOST):
        errors = {}
        EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
        if len(reqPOST['first_name']) < 2:
            errors['first_name'] = "first name must be at least 2 chars"
        if len(reqPOST['last_name']) < 2:
            errors['last_name'] = "first name must be at least 2 chars"
        if not EMAIL_REGEX.match(reqPOST['email']):
            errors['email'] = "email must be valid format"
        if len(reqPOST['password']) < 6:
            errors['password'] = 'password must be at least 8 chars'
        if reqPOST['password'] != reqPOST['confirm_password']:
            errors['confirm_password'] = "passwords do not match"
        return errors


class User(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()

class Message(models.Model):
    text = models.TextField()
    user = models.ForeignKey(User, related_name="owned_mess", on_delete=models.CASCADE)
    user_likes = models.ManyToManyField(User, related_name="liked_posts")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    #objects = MessageManager()

class Comment(models.Model):
    comment = models.TextField()
    user = models.ForeignKey(User, related_name='user_comments', on_delete=models.CASCADE)
    message = models.ForeignKey(Message, related_name="post_comments", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)



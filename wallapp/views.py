from django.shortcuts import render, HttpResponse, redirect
from django.contrib import messages
from .models import *
import bcrypt

def index(request):
    # return HttpResponse("you did")
    return render(request, "index.html")

def register(request):
    if request.method == "POST":
        errors = User.objects.create_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            hashed_pw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt()).decode()
            print(hashed_pw)
            user = User.objects.create(first_name=request.POST['first_name'], last_name=request.POST['last_name'], email=request.POST['email'], password=hashed_pw)
            request.session['user_id'] = user.id
            return redirect('/main_page')
    return redirect('/')

def login(request):
    user = User.objects.filter(email=request.POST['email'])
    if len(user) > 0:
        user = user[0]
        if bcrypt.checkpw(request.POST['password'].encode(), user.password.encode()):
            request.session['user_id'] = user.id
            return redirect('/main_page')
    messages.error(request, "email or pass is incorrect")
    return redirect('/')

def main_page(request):
    if 'user_id' not in request.session:
        messages.error(request, "you need to register or login!")
        return redirect('/')
    context = {
        'user': User.objects.get(id=request.session['user_id']),
        'user_messages': Message.objects.all(),
    }
    return render(request, "main_page.html", context)

def logout(request):
    request.session.clear()
    return redirect('/')

def add_message(request):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        message = Message.objects.create(text=request.POST['text'], user=User.objects.get(id=request.session['user_id']))
        return redirect('/main_page')
    return redirect('/main_page')

def add_comment(request, id):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        user = User.objects.get(id=request.session['user_id'])
        message = Message.objects.get(id=id)
        Comment.objects.create(comment=request.POST['comment'], user=user, message=message)
        return redirect('/main_page')
    return redirect('/main_page')

def like(request, id):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "GET":
        message_with_id = Message.objects.filter(id=id)
        if len(message_with_id) > 0:
            message = Message.objects.get(id=id)
            user = User.objects.get(id=request.session['user_id'])
            message.user_likes.add(user)
            #user.voted_koalas.add(koala)
    return redirect('/main_page')

from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('register', views.register),
    path('login', views.login),
    path('main_page', views.main_page),
    path('logout', views.logout),
    path('add_message', views.add_message),
    path('add_comment/<int:id>', views.add_comment),
    path('message/cast_like/<int:id>', views.like),
]
